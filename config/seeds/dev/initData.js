exports.seed = function (knex) {
  return knex('customers').insert([
    {
      id: '541d79a3-016c-4d86-a31e-2d76e2125cf5',
      birthDate: new Date(2000, 3, 4)
    },
    {
      id: '001d79a3-016c-4d86-a31e-2d76e2125cf6',
      birthDate: new Date(1993, 0, 18)
    }
  ]);
};
