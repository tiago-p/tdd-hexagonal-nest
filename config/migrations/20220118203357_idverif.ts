import {Knex} from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema
        .createTable("customers", function (t) {
            t.uuid("id").primary();
            t.date("birthDate").notNullable();
        })
        .createTable("bookings", function (t) {
            t.uuid("id").primary();
            t.uuid("customer_uuid").notNullable();
            t.string("start_point").notNullable();
            t.string("end_point").notNullable();
            t.string("driver_firstname").notNullable();
            t.decimal("price").notNullable();
            t.dateTime("effective_date").notNullable();
        });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema
        .dropTable("customers")
        .dropTable("bookings");
}
