var config = require('./jest');
config.testRegex = '(/src/.(.*)spec.*|\\.((.*).test|(.*).spec))\\.(ts|js)$';
config.collectCoverageFrom.push('!**/*.spec.(t|j)s');
module.exports = config;
