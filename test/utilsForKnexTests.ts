import knex, { Knex } from 'knex';
import knexConfig from '../config/knexfile';
const BlueBirdPromise = require('bluebird');

const tables = ['customers', 'bookings'];

export function resetDB(sqlConnection: Knex) {
  return sqlConnection.migrate.latest().then(() => {
    return BlueBirdPromise.each(tables, function (table) {
      return sqlConnection.raw('truncate table ' + table + ' cascade');
    });
  });
}

export const sqlConnection = knex(knexConfig.test);
