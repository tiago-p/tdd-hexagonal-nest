export interface PriceGateway {
  computePrice(
    customerId: string,
    startPoint: string,
    endPoint: string,
  ): Promise<number>;
}
