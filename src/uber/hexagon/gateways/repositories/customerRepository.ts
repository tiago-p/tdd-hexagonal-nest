import { Customer } from '../../models/customer';
export interface CustomerRepository {
  findOne(customerId: string): Promise<Customer | null>;
}
