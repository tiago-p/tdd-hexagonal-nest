import { BookingRepository } from '../../gateways/repositories/bookingRepository';
import { CustomerRepository } from '../../gateways/repositories/customerRepository';
import { PriceGateway } from '../../gateways/priceGateway';
import { DateTimeProvider } from '../../gateways/dateTimeProvider';
import { Trip } from '../../models/trip';

export class BookUber {
  constructor(
    private bookingRepository: BookingRepository,
    private customerRepository: CustomerRepository,
    private priceGateway: PriceGateway,
    private dateTimeProvider: DateTimeProvider,
  ) {}

  async handle(trip: Trip, bookingId: string, customerId: string) {
    const customer = await this.customerRepository.findOne(customerId);
    if (!customer) throw new Error('Unknown customer');
    const price = await this.priceGateway.computePrice(
      customerId,
      trip.startPoint,
      trip.endPoint,
    );
    return this.bookingRepository.save({
      id: bookingId,
      customerId,
      startPoint: trip.startPoint,
      endPoint: trip.endPoint,
      driverFirstName: 'Pablo',
      price: customer.isBirthday(this.dateTimeProvider) ? price / 2 : price,
      effectiveDate: this.dateTimeProvider.now(),
    });
  }
}
