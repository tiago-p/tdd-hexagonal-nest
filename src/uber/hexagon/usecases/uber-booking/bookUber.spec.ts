import { Trip } from '../../models/trip';
import { InMemoryBookingRepository } from '../../../adapters/secondary/gateways/repositories/inMemoryUberBookingRepository';
import { InMemoryCustomerRepository } from '../../../adapters/secondary/gateways/repositories/inMemoryCustomerRepository';
import { Customer } from '../../models/customer';
import { InMemoryPriceGateway } from '../../../adapters/secondary/gateways/inMemoryPriceGateway';
import { DeterministicDateTimeProvider } from '../../../adapters/secondary/gateways/DeterministicDateTimeProvider';
import { BookUber } from './bookUber';

describe('Book an Uber', () => {
  let bookingRepository: InMemoryBookingRepository;
  let customerRepository: InMemoryCustomerRepository;
  let priceGateway: InMemoryPriceGateway;
  const dateTimeProvider = new DeterministicDateTimeProvider(
    new Date(2019, 3, 4, 4, 4),
  );
  const nextBookingId = '456def';
  const trip: Trip = {
    startPoint: '8 avenue Foch, Paris',
    endPoint: '8 place de la Madeleine, Paris',
  };

  beforeEach(() => {
    bookingRepository = new InMemoryBookingRepository();
    customerRepository = new InMemoryCustomerRepository();
    priceGateway = new InMemoryPriceGateway();
    customerRepository.addCustomers(
      new Customer('1', new Date(2020, 3, 4)),
      new Customer('2', new Date(2019, 3, 4)),
    );
  });

  describe('Customer does not exist', () => {
    it('should not book any uber for an unknown customer', async () => {
      await expect(bookAnUber('34')).rejects.toThrow('Unknown customer');
      expectNoBooking();
      expectNoPriceComputed();
    });
  });

  describe('Customer has enough money for a booking', () => {
    describe('Today is not the birthday of the customer', () => {
      it('should book an uber effectively', async () => {
        await bookAnUber('1');
        expectEffectiveBookings(20, '1');
      });
    });

    describe('Today is the birthday of the customer', () => {
      it('should book an uber effectively', async () => {
        await bookAnUber('2');
        expectEffectiveBookings(10, '2');
      });
    });

    it('should compute the actual price for the customer regarding the trip', async () => {
      await bookAnUber('2');
      expectPriceComputedWith('2', trip.startPoint, trip.endPoint);
    });
  });

  const bookAnUber = (customerId: string) => {
    return new BookUber(
      bookingRepository,
      customerRepository,
      priceGateway,
      dateTimeProvider,
    ).handle(trip, nextBookingId, customerId);
  };

  const expectEffectiveBookings = (price: number, customerId: string) =>
    expect(bookingRepository.bookings).toEqual([
      {
        id: nextBookingId,
        customerId,
        startPoint: '8 avenue Foch, Paris',
        endPoint: '8 place de la Madeleine, Paris',
        driverFirstName: 'Pablo',
        price,
        effectiveDate: new Date(2019, 3, 4, 4, 4),
      },
    ]);

  const expectNoBooking = () =>
    expect(bookingRepository.bookings.length).toBe(0);

  const expectPriceComputedWith = (customerId, startPoint, endPoint) =>
    expect(priceGateway.computedPriceFrom()).toEqual({
      customerId,
      startPoint,
      endPoint,
    });

  const expectNoPriceComputed = () =>
    expect(priceGateway.computedPriceFrom()).toEqual(null);
});
