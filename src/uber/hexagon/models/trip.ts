export interface Trip {
  startPoint: string;
  endPoint: string;
}
