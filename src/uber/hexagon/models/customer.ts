import { DateTimeProvider } from '../gateways/dateTimeProvider';

export class Customer {
  constructor(private _id: string, private birthDate: Date) {}

  isBirthday(dateTimeProvider: DateTimeProvider): boolean {
    const currentDate = dateTimeProvider.now();
    return (
      this.birthDate.getMonth() === currentDate.getMonth() &&
      this.birthDate.getDay() === currentDate.getDay()
    );
  }

  hasId(id: string) {
    return this._id === id;
  }
}
