export interface Booking {
  id: string;
  customerId: string;
  startPoint: string;
  endPoint: string;
  driverFirstName: string;
  price: number;
  effectiveDate: Date;
}
