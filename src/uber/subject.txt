US : En tant que consommateur, je veux réserver un Uber afin de réaliser course de mon point A vers un point B.

- Si j’ai assez d’argent dans mon compte bancaire, alors ma réservation est effective et je suis débité du prix X connu du système.
         - Si c’est mon jour d’anniversaire, alors je suis débité du prix X/2.
- Si je n’ai pas assez d’argent dans mon compte bancaire, alors ma réservation n’est pas possible


Scenario : Enough money and not my birthday
         Given I am Bruno as a customer
         And I have 30 € TTC in my banking account linked to Uber app
         And today ’15/10/2022’ is not my birthday
         When I attempt to book an Uber:
               | startPoint           | endPoint                       | driverFirstName   | price                |
               | 8 avenue Foch, Paris | 8 place de la Madeleine, Paris |       Pablo       |   20 € HT
         Then the booking should be effective