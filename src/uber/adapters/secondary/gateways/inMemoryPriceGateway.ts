import { PriceGateway } from '../../../hexagon/gateways/priceGateway';

export class InMemoryPriceGateway implements PriceGateway {
  private _computedPriceFrom: {
    customerId: string;
    startPoint: string;
    endPoint: string;
  } | null = null;

  computePrice(
    customerId: string,
    startPoint: string,
    endPoint: string,
  ): Promise<number> {
    this._computedPriceFrom = {
      customerId,
      startPoint,
      endPoint,
    };
    return Promise.resolve(20);
  }

  computedPriceFrom() {
    return this._computedPriceFrom;
  }
}
