import { DateTimeProvider } from '../../../hexagon/gateways/dateTimeProvider';

export class RealDateTimeProvider implements DateTimeProvider {
  now(): Date {
    return new Date();
  }
}
