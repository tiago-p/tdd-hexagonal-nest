import { DateTimeProvider } from '../../../hexagon/gateways/dateTimeProvider';

export class DeterministicDateTimeProvider implements DateTimeProvider {
  constructor(private _date: Date) {}

  now(): Date {
    return this._date;
  }
}
