import {resetDB, sqlConnection} from "../../../../../../../test/utilsForKnexTests";
import {BookingRepository} from "../../../../../hexagon/gateways/repositories/bookingRepository";
import {KnexBookingRepository} from "./knexBookingRepository";
import {Booking} from "../../../../../hexagon/models/booking";

describe('Booking repository implementation with Knex', () => {

    let knexBookingRepository: BookingRepository;

    beforeEach(async () => {
        await resetDB(sqlConnection);
        knexBookingRepository = new KnexBookingRepository(sqlConnection);
    });

    afterAll(async () => {
        await sqlConnection.destroy();
    });

    it('should be able to save a booking', async () => {
        const customerBirthDate = new Date(2021, 3, 4);
        await sqlConnection("customers").insert([{
            id: "24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee",
            birthDate:  customerBirthDate
        }]);
        const booking: Booking = {
            id: "6337122a-6356-4112-8575-cc7b84d6c3ea",
            customerId: '24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee',
            startPoint: '8 avenue Foch',
            endPoint: '9 avenue Foch',
            price: 20,
            effectiveDate: new Date(2022, 3, 4, 4, 5),
            driverFirstName: 'Fouvlo'
        }
        await knexBookingRepository.save(booking);
        const res = await sqlConnection('bookings')
            .select('id',
                'customer_uuid',
                'start_point',
                'end_point',
                'price',
                'effective_date',
                'driver_firstname')
            .first();
        expect(res).toEqual({
            id: "6337122a-6356-4112-8575-cc7b84d6c3ea",
            customer_uuid: '24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee',
            start_point: '8 avenue Foch',
            end_point: '9 avenue Foch',
            price: '20.00',
            effective_date: new Date(2022, 3, 4, 4, 5),
            driver_firstname: 'Fouvlo'
        });
    });

});