import {Knex} from "knex";
import {BookingRepository} from "../../../../../hexagon/gateways/repositories/bookingRepository";
import {Booking} from "../../../../../hexagon/models/booking";

export class KnexBookingRepository implements BookingRepository {

    constructor(private sqlConnection: Knex) {
    }

    save(booking: Booking): Promise<void> {
        return this.sqlConnection('bookings').insert({
            id: booking.id,
            customer_uuid: booking.customerId,
            start_point: booking.startPoint,
            end_point: booking.endPoint,
            driver_firstname: booking.driverFirstName,
            effective_date: booking.effectiveDate,
            price: booking.price
        });
    }


}