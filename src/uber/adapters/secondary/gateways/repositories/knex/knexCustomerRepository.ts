import {CustomerRepository} from "../../../../../hexagon/gateways/repositories/customerRepository";
import {Knex} from "knex";
import {Customer} from "../../../../../hexagon/models/customer";

export class KnexCustomerRepository implements CustomerRepository {

    constructor(private sqlConnection: Knex) {
    }

    async findOne(customerId: string): Promise<Customer | null> {
        const res = await this.sqlConnection('customers')
            .select('id', 'birthDate')
            .where({id: customerId})
            .first();
        return new Customer(
            res['id'],
            res['birthDate']
        )
    }

}