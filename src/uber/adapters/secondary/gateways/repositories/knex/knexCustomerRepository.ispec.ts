import {KnexCustomerRepository} from "./knexCustomerRepository";
import {resetDB, sqlConnection} from "../../../../../../../test/utilsForKnexTests";
import {CustomerRepository} from "../../../../../hexagon/gateways/repositories/customerRepository";
import {Customer} from "../../../../../hexagon/models/customer";

describe('Customer repository implementation with Knex', () => {

    let knexCustomerRepository: CustomerRepository;

    beforeEach(async () => {
        await resetDB(sqlConnection);
        knexCustomerRepository = new KnexCustomerRepository(sqlConnection);
    });

    afterAll(async () => {
        await sqlConnection.destroy();
    });

    it('should be able to find a customer by its id', async () => {
        const customerBirthDate = new Date(2021, 3, 4);
        await sqlConnection("customers").insert([{
            id: "24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee",
            birthDate:  customerBirthDate
        }]);
        expect(await knexCustomerRepository.findOne("24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee"))
            .toEqual(
                new Customer(
                    "24f7bdbe-77f0-4fb9-b5e0-a87fd8b30bee",
                    customerBirthDate
                )
            )
    });

});