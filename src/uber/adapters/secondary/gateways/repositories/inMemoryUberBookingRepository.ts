import {BookingRepository} from "../../../../hexagon/gateways/repositories/bookingRepository";
import {Booking} from "../../../../hexagon/models/booking";

export class InMemoryBookingRepository implements BookingRepository {

    constructor(private _bookings: Booking[] = []) {}

    async save(booking: Booking) {
        this._bookings.push(booking);
    }

    get bookings(): Booking[] {
        return this._bookings;
    }
}