import {CustomerRepository} from '../../../../hexagon/gateways/repositories/customerRepository';
import {Customer} from '../../../../hexagon/models/customer';

export class InMemoryCustomerRepository implements CustomerRepository {

    constructor(private _customers: Customer[] = []) {
    }

    async findOne(customerId: string): Promise<Customer | null> {
        return Promise.resolve(this._customers.find(c => c.hasId(customerId)) || null);
    }

    addCustomers(...customers: Customer[]) {
        for (const customer of customers)
            this._customers.push(customer);
    }
}