import { Body, Controller, Post } from '@nestjs/common';
import { BookUber } from '../../../hexagon/usecases/uber-booking/bookUber';

@Controller()
export class UberAppController {
  constructor(private readonly bookUber: BookUber) {}

  @Post('bookings')
  async bookAnUber(@Body() params: BookingParam): Promise<void> {
    const { startPoint, endPoint, bookingId, customerId } = params;
    await this.bookUber.handle({ startPoint, endPoint }, bookingId, customerId);
  }
}

interface BookingParam {
  bookingId: string;
  customerId: string;
  startPoint: string;
  endPoint: string;
}
