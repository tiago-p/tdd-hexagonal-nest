import { Module } from '@nestjs/common';
import { UberAppController } from './uberApp.controller';
import { BookUber } from '../../../hexagon/usecases/uber-booking/bookUber';
import { InMemoryPriceGateway } from '../../secondary/gateways/inMemoryPriceGateway';
import { RealDateTimeProvider } from '../../secondary/gateways/RealDateTimeProvider';
import { KnexBookingRepository } from '../../secondary/gateways/repositories/knex/knexBookingRepository';
import knex from 'knex';
import knexConfig from '../../../../../config/knexfile';
import { KnexCustomerRepository } from '../../secondary/gateways/repositories/knex/knexCustomerRepository';

const knexConnection = knex(knexConfig.development);
@Module({
  imports: [],
  controllers: [UberAppController],
  providers: [
    {
      provide: BookUber,
      useFactory: (
        bookingRepository,
        customerRepository,
        priceGateway,
        dateTimeProvider,
      ) =>
        new BookUber(
          bookingRepository,
          customerRepository,
          priceGateway,
          dateTimeProvider,
        ),
      inject: [
        'BookingRepository',
        'CustomerRepository',
        'PriceGateway',
        'DateTimeProvider',
      ],
    },
    {
      provide: 'BookingRepository',
      useFactory: () => {
        return new KnexBookingRepository(knexConnection);
      },
    },
    {
      provide: 'CustomerRepository',
      useFactory: () => {
        /* const customer1 = new Customer(
                    '66dfdd73-c2b8-42db-b4ce-e6b2599db86c',
                    new Date(2022, 0, 18, 3, 4)
                )*/
        /*const customerRepository = new InMemoryCustomerRepository();
                customerRepository.addCustomers(customer1);
                return customerRepository;*/
        return new KnexCustomerRepository(knexConnection);
      },
    },
    {
      provide: 'PriceGateway',
      useFactory: () => {
        return new InMemoryPriceGateway();
      },
    },
    {
      provide: 'DateTimeProvider',
      useFactory: () => {
        return new RealDateTimeProvider();
      },
    },
  ],
})
export class UberAppModule {}
