import { NestFactory } from '@nestjs/core';
import { UberAppModule } from './uber/adapters/primary/controllers/uberApp.module';

async function bootstrap() {
  const app = await NestFactory.create(UberAppModule);
  await app.listen(3000);
}
bootstrap();
